[![Actions Status](https://github.com/abelcarreras/kimonet/actions/workflows/python-package.yml/badge.svg)](https://github.com/abelcarreras/kimonet/actions)
[![PyPI version](https://badge.fury.io/py/kimonet.svg)](https://badge.fury.io/py/kimonet)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/abelcarreras/kimonet/)

Kimonet
=======
A kinetic Monte Carlo code to simulate exciton dynamics

Requirements
------------
- Python 2.7.x/3.2+ 
- numpy
- scipy
- matplotlib
- networkx
- h5py
- pygraphviz (which requires graphviz-dev library) [optional]

Contact info
------------
Abel Carreras  
abelcarreras83@gmail.com

Donostia International Physics Center (DIPC)  
Donostia-San Sebastian, Euskadi (Spain)