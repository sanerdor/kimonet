import numpy as np
import sys
sys.path.insert(1, '/home/sanerdor/programming/kimonet')

from kimonet.system.state import ground_state as gs
from kimonet.system.state import State

from kimonet.core.processes.transitions import Transition

from kimonet.system.molecule import Molecule
from kimonet.system.generators import crystal_system

from kimonet.system.vibrations import MarcusModel
from kimonet.core.processes.couplings import dexter_coupling
from kimonet.core.processes.couplings import dexter_coupling_k_adapted
from kimonet.core.processes.decays import einstein_radiative_decay
from kimonet.core.processes.types import GoldenRule, DecayRate

from kimonet import calculate_kmc, calculate_kmc_parallel

from kimonet.analysis import TrajectoryAnalysis
from kimonet.analysis import visualize_system

from kimonet.core.processes.types import SimpleRate

if __name__ == '__main__':

    # states list
    gs = gs.copy()
    t1 = State(label = 't1',
               energy = 1.23)
        
    # transition list
    transitions = [Transition(t1,gs,
                              tdm = [0.0,0.0],
                              reorganization_energy = 0.09)]
    
    # system generator
    molecule = Molecule(site_energy = 0.0)
    
    molecule0 = molecule.copy()
    molecule0.label = 0
    
    molecule1 = molecule.copy()
    molecule1.label = 1
                
    system = crystal_system(molecules = [molecule0,
                                         molecule1],
                            unitcell = [[7.17,0.0],
                                        [0.0,14.211]],
                            scaled_site_coordinates = [[0.0,0.0],
                                                       [0.5,0.5]],
                            dimensions = [5,5],
                            orientations = [[0,0,-0.5508],
                                            [0,0,0.6345]])
    
    system.add_excitation_index(t1,np.random.randint(0,2*25))
    
    #visualize_system(system)

    # process generator
    system.process_scheme = [GoldenRule(initial_states = (t1,gs),
                                        final_states = (gs,t1),
                                        electronic_coupling_function = dexter_coupling_k_adapted,
                                        description = 'Dexter coupling',
                                        arguments = {'k_factor': [[8.130,0.190],
                                                                  [0.190,8.130]]},
                                        vibrations = MarcusModel(transitions=transitions))
                            ]
    
    system.cutoff_radius = 8.0
    
    # compute trajectories
    trajectories = calculate_kmc(system,
                                 num_trajectories = 1,
                                 max_steps = 100,
                                 silent = False)
    """ trajectories = calculate_kmc_parallel(system,
                                          num_trajectories = 14,
                                          max_steps = 1000,
                                          silent = False, 
                                          processors = 14) """
    
    # trajectory analysis
    analysis = TrajectoryAnalysis(trajectories)
    print('diffusion coefficient: {:9.5e} Angs^2/ns'.format(analysis.diffusion_coefficient()))
    print('lifetime:              {:9.5e} ns'.format(analysis.lifetime()))
    print('diffusion length:      {:9.5e} Angs'.format(analysis.diffusion_length()))
    print('diffusion tensor (angs^2/ns)')
    
    for state in analysis.get_states():
        print('\nState: {}\n--------------------------------'.format(state))
        print('diffusion coefficient: {:9.5e} Angs^2/ns'.format(analysis.diffusion_coefficient(state)))
        print('lifetime:              {:9.5e} ns'.format(analysis.lifetime(state)))
        print('diffusion length:      {:9.5e} Angs'.format(analysis.diffusion_length(state)))
        print('diffusion tensor (angs^2/ns)')
        
    analysis.plot_2d('t1').show()